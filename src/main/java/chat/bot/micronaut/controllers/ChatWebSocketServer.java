package chat.bot.micronaut.controllers;

import io.micronaut.websocket.WebSocketBroadcaster;
import io.micronaut.websocket.WebSocketSession;
import io.micronaut.websocket.annotation.OnClose;
import io.micronaut.websocket.annotation.OnMessage;
import io.micronaut.websocket.annotation.OnOpen;
import io.micronaut.websocket.annotation.ServerWebSocket;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ServerWebSocket("/chatWebSocket/{topic}")
public class ChatWebSocketServer {
    private WebSocketBroadcaster broadcaster;

    public ChatWebSocketServer(WebSocketBroadcaster broadcaster) {
        this.broadcaster = broadcaster;
    }

    @OnOpen
    public void onOpen(WebSocketSession session, String topic) {
        log.info("Connection established with client");
        broadcaster.broadcastSync("Hello I'm a chat bot, ask a question");
    }

    @OnMessage
    public void onMessage(WebSocketSession session, String topic, String message) {
        log.info("Message received, starting processing...");
    }

    @OnClose
    public void onClose(WebSocketSession session, String topic) {
        log.error("Closing the connection with the client...");
    }

}