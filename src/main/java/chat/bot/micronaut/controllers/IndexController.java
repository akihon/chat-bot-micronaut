package chat.bot.micronaut.controllers;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.views.View;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;

@Controller("/index")
public class IndexController {

    @View("index")
    @Get("/{topic}")
    public HttpResponse<TopicHandler> index(HttpRequest<?> request, @PathVariable String topic) {
        val socketAddress = request.getServerAddress();
        val topicHandler = new TopicHandler(socketAddress.getHostName(),
                socketAddress.getPort(),
                topic);
        return HttpResponse.ok(topicHandler);
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public final class TopicHandler {
        public String host;
        public int port;
        public String topic;
    }
}