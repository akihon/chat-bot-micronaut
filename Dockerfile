FROM oracle/graalvm-ce:19.2.0.1 as graalvm
COPY . /home/app/chat-bot-micronaut
WORKDIR /home/app/chat-bot-micronaut
RUN gu install native-image
RUN native-image --no-server -cp target/chat-bot-micronaut-*.jar

FROM frolvlad/alpine-glibc
EXPOSE 8080
COPY --from=graalvm /home/app/chat-bot-micronaut .
ENTRYPOINT ["./chat-bot-micronaut"]
